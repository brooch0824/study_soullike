using UnityEditor.ProjectWindowCallback;
using UnityEditorInternal;
using UnityEngine;

public class KeyBoardInput : IUserInput
{
    //其实可以通过inputaction实现的
    //不过考虑到自己的代码水平比较一版，还是手写一下长长知识
    [Header("======== Key Settings ========")]
    public string keyUp = "w";
    public string keyDown = "s";
    public string keyLeft = "a";
    public string keyRight = "d";

    public string keyA = "space";
    public string keyB = "f";
    public string keyD = "Mouse X";
    public string keyE = "Mouse Y";

    public string keyF = "Mouse ScrollWheel";

    public string keyG = "mouse 0";//左键
    public string keyH = "mouse 1";//右键
    public string keyI = "mouse 2";//中键


    public MyButton keyBoardA = new MyButton();
    public MyButton keyBoardB = new MyButton();
    public MyButton keyBoardG = new MyButton();
    public MyButton keyBoardH = new MyButton();

    public MyButton keyBoardI = new MyButton();
    MyTimer myTimer = new MyTimer();
    Vector2 tempDAxis;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        print(Cursor.visible + " " + Cursor.lockState);

        keyBoardA.Tick(Input.GetKey(keyA));
        keyBoardB.Tick(Input.GetKey(keyB));
        keyBoardG.Tick(Input.GetKey(keyG));
        keyBoardH.Tick(Input.GetKey(keyH));
        keyBoardI.Tick(Input.GetKey(keyI));

        //print(keyBoardA.IsExtending && keyBoardA.OnPressed);


        //如果不能移动，向量输入就为0？不对，应该是继承之前的目标
        if (canMove == true)
        {
            targetDup = Input.GetAxisRaw("Vertical");
            //(Input.GetKey(keyUp) ? 1.0f : 0) - (Input.GetKey(keyDown) ? 1.0f : 0);
            targetDright = Input.GetAxisRaw("Horizontal");
            //Input.GetKey(keyRight) ? 1.0f : 0 - (Input.GetKey(keyLeft) ? 1.0f : 0);
        }

        Dup = Mathf.SmoothDamp(Dup, targetDup, ref velocityDup, 0.1f);
        Dright = Mathf.SmoothDamp(Dright, targetDright, ref velocityDright, 0.1f);

        tempDAxis = SquareToCircle(new Vector2(Dright, Dup));

        Dmag = Mathf.Sqrt(tempDAxis.y * tempDAxis.y + tempDAxis.x * tempDAxis.x);//输入标量
        Dvec = tempDAxis.x * transform.right + tempDAxis.y * transform.forward;//标量*向量 + 标量*向量，进行方向赋值

        JX = Input.GetAxisRaw(keyD);
        JY = Input.GetAxisRaw(keyE);
        targetScroll = Input.GetAxisRaw(keyF);


        //btn按下的时候，计时器启动，计时之内btnup就翻滚，计时结束之后还没翻滚就奔跑,然后检测到btnup就停止
        if (keyBoardA.OnPressed)
        {
            StartTimer(myTimer, 0.2f);
        }
        if (keyBoardA.IsPressing)
        {
            myTimer.Tick();
            //Debug.Log("mytimerelapsedtime:" + myTimer.elapsedTime);
        }
        if (myTimer.state == MyTimer.STATE.RUN && keyBoardA.OnReleased)
        {
            roll = keyBoardA.OnReleased;
            myTimer.state = MyTimer.STATE.FINISHED;
        }
        else
        {
            roll = false;
        }
        //Debug.Log("run:" + run);

        run = keyBoardA.IsPressing && !roll;
        defense = keyBoardH.IsPressing;
        jump = keyBoardB.OnPressed;
        attack = keyBoardG.OnPressed;
        tryLock = keyBoardI.OnPressed;
    }

    /// <summary>
    /// 计时器开启（开启代表着只有一帧执行就好了喔）
    /// </summary>
    /// <param name="myTimer"></param>
    /// <param name="duration"></param>
    void StartTimer(MyTimer myTimer, float duration)
    {
        myTimer.duration = duration;
        myTimer.Go();
    }

}
