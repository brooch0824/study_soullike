using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftArmAnimFix : MonoBehaviour
{

    private Animator anim;
    public Vector3 leftArmEuler;

    void Awake()
    {

        anim = GetComponent<Animator>();
    }

    void OnAnimatorIK()
    {
        //这逼玩意儿好像是只执行一帧啊
        if (!anim.GetBool("defense"))
        {
            Transform leftArm = anim.GetBoneTransform(HumanBodyBones.LeftLowerArm);
            leftArm.localEulerAngles += leftArmEuler;
            anim.SetBoneLocalRotation(HumanBodyBones.LeftLowerArm, Quaternion.Euler(leftArm.localEulerAngles));
        }

    }
}
