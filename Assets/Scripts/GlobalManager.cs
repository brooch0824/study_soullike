using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalManager : MonoBehaviour
{
    public float timeDay;
    public GameObject sunLight;


    void FixedUpdate()
    {
        sunLight.transform.Rotate(new Vector3(1,0,0), 360 / timeDay * Time.deltaTime);
    }
}
