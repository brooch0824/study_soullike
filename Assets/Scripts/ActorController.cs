using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]//给对象挂载一个rigidbody
public class ActorController : IUserInput
{
    public GameObject model;
    public CameraController camCon;
    public IUserInput pi;
    public Animator anim;
    public PhysicMaterial fictionOne;
    public PhysicMaterial fictionZero;

    public float jumpVelocity;
    public float rollVelocity;

    [SerializeField] private float walkSpeed;
    [SerializeField] private float runMultiplier;
    private float speedMultiplier;
    private float planarVecMultiplier = 1.0f;//平面移动速度倍率
    public float smoothRun;
    [SerializeField] private float timeFall;
    private float targetRunMultiAnim;//渐入奔跑状态的效率

    private Rigidbody rigid;
    //rigid无法在update里调用，只能在fixedupdate里面做
    //物理引擎预设的模拟频率是每秒50次，速度是不一样的，不能在update里，周期是不一致的

    private Vector3 planarVec;//平面移动速度
    private Vector3 thrustVec;//冲量（加速度）增量
    private bool canAttack = true;//是否可以攻击
    private bool canDefense = true;//是否允许防御

    private Vector3 deltaPos;//动画移动距离


    //awake的时候把所有组件装好
    //start继awake之后运行，此时进行组件之间的调用就能避免出现missing的问题

    void Awake()
    {
        IUserInput[] inputs = GetComponents<IUserInput>();
        foreach (var input in inputs)
        {
            if (input == true)
            {
                pi = input;
                break;
            }

        }
        anim = model.GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
    }
    void Start()
    {
        rigid.constraints = RigidbodyConstraints.FreezeRotation;//冻结子物体模型旋转
    }

    // Update is called once per frame
    void Update()
    {
        GoLayerSet();
        //Debug.Log("planarVecMultiplier" + planarVecMultiplier);
        anim.SetFloat("forward",
        Mathf.Lerp(
            anim.GetFloat("forward"),
             planarVecMultiplier * pi.Dmag * (pi.run ? 2.0f : 1.0f),
                (pi.Dmag > 0.5f ? 0.1f : 0.015f)));


        anim.SetFloat("timeFall", timeFall);

        anim.SetBool("defense", canDefense && pi.defense);

        //anim.Getfloat:取状态机某个值
        if (pi.jump)
        {
            anim.SetTrigger("jump");
        }
        if (pi.roll)
        {
            anim.SetTrigger("roll");
        }
        if (pi.attack)
        {
            anim.SetTrigger("attack");
        }


        if (anim.GetBool("isground"))
        {
            timeFall = 0;
        }
        else
        {
            timeFall += Time.deltaTime;
        }


        if (pi.tryLock)
        {
            camCon.TryLockOnEnemy();
        }

        if (pi.Dmag > 0.1f)//如果输入量太低就不旋转角色，免得出现向量的标量为0无法确定方向的问题
        {
            model.transform.forward = Vector3.Slerp(model.transform.forward, pi.Dvec, pi.roll ? 1 : 0.2f);//转身插值
        }
        smoothRun = Mathf.Lerp(
            smoothRun,
                planarVecMultiplier * speedMultiplier * pi.Dmag * (pi.run ? runMultiplier : 1.0f),
                 pi.roll ? 1.0f : (pi.Dmag > 0.5f ? 0.1f : 0.025f));

        // smoothRun = Mathf.SmoothDamp(smoothRun, speedMultiplier * pi.Dmag * ((pi.run) ? runMultiplier : 1.0f), ref currrunMultiplier, 0.4f);
        planarVec = model.transform.forward * smoothRun;


        if (canAttack)//这个是如果可以攻击就set
        {
            anim.SetBool("canAttack", true);

        }
        else
        {
            anim.SetBool("canAttack", false);
        }
        if (canDefense)//这个是只有在真的触发举盾后才执行
        {
            anim.SetBool("canDefense", true);
        }
        else
        {
            anim.SetBool("canDefense", false);
        }

    }

    void FixedUpdate()
    {
        //time.fixeddeltatime
        rigid.position += planarVec * Time.fixedDeltaTime * walkSpeed + deltaPos;
        rigid.velocity += thrustVec;
        thrustVec = Vector3.zero;
        deltaPos = Vector3.zero;
    }

    /// <summary>
    /// 判断状态机在哪个状态，默认layername是baselayer
    /// </summary>
    public bool CheckState(string statename, string layername = "Base Layer")
    {
        return anim.GetCurrentAnimatorStateInfo(anim.GetLayerIndex(layername)).IsName(statename);
    }



    /// <summary>
    /// 触发跳跃动画的时候触发的事件
    /// </summary>
    public void OnJumpEnter()
    {
        pi.canMove = false;
        canDefense = false;
        thrustVec = new Vector3(0, jumpVelocity, 0);

        pi.GetComponent<CapsuleCollider>().material = fictionZero;
    }


    ///<summary>
    ///在地面触发的事件
    ///</summary>
    public void OnGroundEnter()
    {
        planarVecMultiplier = 0;
        pi.canMove = true;
        planarVecMultiplier = 1;
        canAttack = true;
        canDefense = true;
    }

    /// <summary>
    /// 滞空的那一帧触发的事件
    /// </summary>
    public void OnFallEnter()
    {
        pi.canMove = true;
        planarVecMultiplier = 0.4f;
    }

    /// <summary>
    /// 翻滚的时候触发的事件
    /// </summary>
    public void OnRollEnter()
    {
        rigid.velocity = Vector3.zero;
        pi.canMove = false;
        planarVecMultiplier = 0;
        canAttack = false;
        canDefense = false;
        anim.SetTrigger("roll");
        if (!pi.run)
        {
            thrustVec = model.transform.forward * rollVelocity;//翻滚的时候会有个初始加速度
        }

    }

    ///<summary>
    ///很高的地方坠地动画开始的时候触发的事件
    ///</summary>
    public void OnDeepFallEnter()
    {
        pi.canMove = false;
        canAttack = false;
        canDefense = false;
        planarVecMultiplier = 0;
        smoothRun = 0;
        layerWeight = 0;
        planarVec = Vector3.zero;
    }


    ///<summary>
    ///退出攻击状态回到idle时
    ///</summary>
    public void OnAttackIdleEnter()
    {
        animLayerSetAttack = new float[] { 0, 0.05f };
        pi.canMove = true;
        canDefense = true;
    }

    ///<summary>
    ///离开idle进入攻击状态时
    ///</summary>
    public void OnAttackIdleExit()
    {
        animLayerSetAttack = new float[] { 1, 0.1f };
        planarVecMultiplier = 0;
        planarVecMultiplier = 1;
        pi.canMove = false;
        canDefense = false;
    }

    ///<summary>
    ///退出防御状态回到idle时
    ///</summary>
    public void OnDefenseIdleEnter()
    {
        //anim.SetLayerWeight(anim.GetLayerIndex("defense"), 0);

        animLayerSetDefense = new float[] { 0, 0.01f };
        // layerName = "defense";
        // layerWeightLerp = 0.01f;
        // layerWeight = 0;
        speedMultiplier = 1f;
    }

    ///<summary>
    ///离开idle进入防御状态时
    ///</summary>
    public void OnDefenseIdleExit()
    {
        //anim.SetLayerWeight(anim.GetLayerIndex("attack"), 0);
        animLayerSetAttack = new float[] { 0, 1 };
        animLayerSetDefense = new float[] { 1, 0.1f };
        // layerName = "defense";
        // layerWeightLerp = 0.1f;
        // layerWeight = 1;
        speedMultiplier = 0.7f;
    }

    ///<summary>
    ///碰撞到Ground
    ///</summary>
    public void IsGround()
    {
        anim.SetBool("isground", true);
        //给予摩擦力，不然上不去斜坡
        pi.GetComponent<CapsuleCollider>().material = fictionOne;
        // if (pi.Dmag < 0.1f)
        // {
        //     rigid.velocity = Vector3.Lerp(rigid.velocity, Vector3.zero, 0.5f);
        // }
    }

    ///<summary>
    ///没碰撞到Ground
    ///</summary>
    public void IsNotGround()
    {
        anim.SetBool("isground", false);
        pi.GetComponent<CapsuleCollider>().material = fictionZero;
    }

    /// <summary>
    /// RootMotion，动画是否移动胶囊
    /// </summary>
    /// <param name="_deltaPos"></param>
    public void OnUpdateRM(object _deltaPos)
    {
        if (CheckState("attack1hA", "attack") || CheckState("attack1hB", "attack") || CheckState("attack1hC", "attack"))
        {
            deltaPos += 0.4f * deltaPos + 1.6f * (Vector3)_deltaPos;//动画的移动只占60%
            //deltaPos += (Vector3)_deltaPos;
        }
    }

    void GoLayerSet()
    {
        OnBaseLayerSet();
        OnAttackLayerSet();
        OnDefenseLayerSet();

    }

    ///<summary>
    ///baseLayer层级权重管理
    ///</summary>
    public void OnBaseLayerSet()
    {
        anim.SetLayerWeight(anim.GetLayerIndex("Base"), Mathf.Lerp(anim.GetLayerWeight(anim.GetLayerIndex("Base")), animLayerSetBase[0], animLayerSetBase[1]));
    }
    ///<summary>
    ///attackLayer层级权重管理
    ///</summary>
    public void OnAttackLayerSet()
    {
        anim.SetLayerWeight(anim.GetLayerIndex("attack"), Mathf.Lerp(anim.GetLayerWeight(anim.GetLayerIndex("attack")), animLayerSetAttack[0], animLayerSetAttack[1]));
    }
    ///<summary>
    ///defenseLayer层级权重管理
    ///</summary>
    public void OnDefenseLayerSet()
    {
        anim.SetLayerWeight(anim.GetLayerIndex("defense"), Mathf.Lerp(anim.GetLayerWeight(anim.GetLayerIndex("defense")), animLayerSetDefense[0], animLayerSetDefense[1]));
    }


    // /// <summary>
    // /// 碰撞的时候
    // /// </summary>
    // /// <param name="other"></param>
    // void OnCollisionStay(Collision other)
    // {
    //     var tag = other.gameObject.tag;
    //     var layer = other.gameObject.layer;


    //     rigid.constraints = RigidbodyConstraints.FreezeRotation;
    //     switch (tag)
    //     {
    //         case "slope":
    //             if (pi.Dmag < 0.3f)
    //             {
    //                 rigid.constraints = RigidbodyConstraints.FreezeAll;
    //             }
    //             break;
    //         case "plane":
    //             break;
    //         default:
    //             break;
    //     }
    //     switch (layer)
    //     {
    //         case 3:
    //             anim.SetBool("isground", true);
    //             break;
    //     }

    // }

    // void OnCollisionExit(Collision other)//碰撞结束的那一帧触发
    // {
    //     anim.SetBool("isground", false);
    // }

}
