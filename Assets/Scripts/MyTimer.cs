using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTimer
{
    public enum STATE
    {
        IDLE,
        RUN,
        FINISHED
    }

    public STATE state;//计时器状态

    public float duration;//长按的目标时间

    public float elapsedTime;//长按的当前持续时间

    /// <summary>
    /// 每次Tick会执行的操作
    /// </summary>
    public void Tick()
    {
        switch (state)
        {
            case STATE.IDLE:
                break;

            case STATE.RUN:
                elapsedTime += Time.deltaTime;
                if (elapsedTime >= duration)
                {
                    state = STATE.FINISHED;
                }
                break;

            case STATE.FINISHED:
                break;

            default:
                Debug.Log("计时器错误");
                break;
        }
    }

    /// <summary>
    /// 计时器开启的通用前置内容
    /// </summary>
    public void Go()
    {
        elapsedTime = 0;
        state = STATE.RUN;
    }


}
