using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : IUserInput
{
    public IUserInput pi;
    public MyUIManager ui;
    public float horizontalSpeed = 3;
    public float verticalSpeed = 1.2f;
    public float lockedEnemyHalfHeight;
    public GameObject lockedEnemy;

    GameObject playerHandle;//直接转Y轴的时候用
    GameObject cameraHandle;//转X轴的时候用
    float tempEuler = 10;
    GameObject model;
    Vector3 tempModelEuler;
    Animator anim;
    float smoothRun;
    float halfBox = 10;
    float mouseScrollOffset;//鼠标缩放

    void Awake()
    {
        cameraHandle = transform.parent.gameObject;
        playerHandle = cameraHandle.transform.parent.gameObject;
        pi = playerHandle.GetComponent<IUserInput>();
        model = pi.GetComponent<ActorController>().model;
        anim = pi.GetComponent<ActorController>().anim;
    }


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // print(mouseScrollOffset + " + " + Input.GetAxisRaw(pi.keyF));
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            if (lockedEnemy == null)
            {
                mouseScrollOffset = Mathf.Clamp(mouseScrollOffset + pi.targetScroll * 0.075f, -0.0075f * 6, 0.0075f * 20);//检测鼠标移动

                smoothRun = pi.GetComponent<ActorController>().smoothRun;

                transform.localPosition = new Vector3(0, 0,
                mouseScrollOffset + Mathf.Lerp(transform.localPosition.z, -5 - smoothRun * 0.8f, 0.015f));//镜头缓动

                tempModelEuler = model.transform.eulerAngles;

                playerHandle.transform.Rotate(Vector3.up, pi.JX * horizontalSpeed);
                //cameraHandle.transform.Rotate(Vector3.right, pi.JY * -verticalSpeed);
                tempEuler += pi.JY * -verticalSpeed;
                cameraHandle.transform.localEulerAngles = new Vector3(Mathf.Clamp(tempEuler, -90, 90), 0, 0);
                model.transform.eulerAngles = tempModelEuler;


            }
            else
            {
                tempModelEuler = lockedEnemy.transform.position - model.transform.position;
                //tempModelEuler.y = 0;//正朝向目标
                //playerHandle.transform.forward = tempModelEuler;
                model.transform.forward = new Vector3(model.transform.forward.x, 0, model.transform.forward.z);
                playerHandle.transform.LookAt(new Vector3(lockedEnemy.transform.position.x, 0, lockedEnemy.transform.position.z));
                print(lockedEnemyHalfHeight);
                LockedEnemyJudge();

            }
            //在X轴方向上上下旋转的时候直接改值会有一个同位角的问题，在角度<0的时候会自动+360，故直接用理论的方法不行
        }
    }

    /// <summary>
    /// 试着锁定敌人
    /// </summary>
    public void TryLockOnEnemy()
    {
        if (lockedEnemy == null)
        {
            lockLockedEnemy();
        }
        else
        {
            UnlockLockedEnemy();
        }

    }

    /// <summary>
    /// 判断是否还要锁定敌人
    /// </summary>
    void LockedEnemyJudge()
    {
        if (ui.distance > halfBox * 2 || ui.distance < 0.5f)
        {
            UnlockLockedEnemy();
        }

    }

    /// <summary>
    /// 解除锁定敌人
    /// </summary>
    void UnlockLockedEnemy()
    {
        ui.textArea.GetComponent<Text>().text = "嗅嗅……敌人在哪快出来……！~";
        //try to release
        lockedEnemy = null;
        playerHandle.transform.eulerAngles = new Vector3(0, playerHandle.transform.eulerAngles.y, 0);
        model.transform.forward = new Vector3(model.transform.forward.x, 0, model.transform.forward.z);//回正角色和相机朝向

    }

    /// <summary>
    /// 锁定敌人
    /// </summary>
    void lockLockedEnemy()
    {
        print(cameraHandle.transform.Find("Main Camera").transform.position + " " + model.transform.position + " " + (cameraHandle.transform.Find("Main Camera").transform.position - model.transform.position).magnitude);
        //try to lock
        //获得碰撞盒的中心坐标（我是拿Main Camera进行判断的，不是角色模型）
        Vector3 boxCenter = transform.position + transform.forward * (halfBox + (cameraHandle.transform.Find("Main Camera").transform.position - model.transform.position).magnitude);//加上playerhandle和角色之间的距离，这样就锁定不到角色后面的怪物了
        //创造碰撞盒（长宽高、旋转）
        Collider[] cols = Physics.OverlapBox(boxCenter, new Vector3(3, 5, halfBox), transform.rotation, LayerMask.GetMask("Enemy"));
        //得到碰撞到的每个单位
        List<GameObject> colss = new List<GameObject>();


        int countNum = 0;
        string countText = null;
        foreach (var col in cols)
        {
            countNum += 1;
            countText = countText + col.gameObject.name + ", ";
        }

        if (countNum == 0)
        {
            ui.textArea.GetComponent<Text>().text = "呜呜，对不起……主人，是我没用，一个敌人都没找到……";
        }
        else
        {
            ui.textArea.GetComponent<Text>().text = string.Format("找到了{0}个目标！！\n", countNum) + countText + "  夸夸嘛~";

            float nearestDistance = float.MaxValue;
            foreach (var col in cols)
            {
                //立刻计算出距离最近的作为锁定目标，后面可能需要将能锁定的敌人从近到远排序
                var distance = Mathf.Sqrt(
                Mathf.Pow(col.transform.position.x - model.transform.position.x, 2) +
                Mathf.Pow(col.transform.position.y - model.transform.position.y, 2) +
                Mathf.Pow(col.transform.position.z - model.transform.position.z, 2));
                if (distance <= nearestDistance)
                {
                    nearestDistance = distance;
                    lockedEnemy = col.gameObject;
                    lockedEnemyHalfHeight = col.bounds.extents.y;
                }

            }
        }
    }
}
