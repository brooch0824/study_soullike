using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;

public class MyUIManager : MonoBehaviour
{
    public GameObject Debug;
    public GameObject textArea;
    public GameObject textArea2;
    public GameObject model;
    public CameraController camCon;
    public Image lockedTargetDot;
    string text;
    public float distance;

    void Awake()
    {
        // lockedTargetDot = transform.Find("lockTargetDot").GetComponent<Image>();
    }
    void Start()
    {
        lockedTargetDot.enabled = false;

    }
    void LateUpdate()
    {
        if (camCon.lockedEnemy != null)
        {
            text = textArea2.GetComponent<Text>().text;
            distance = Mathf.Sqrt(
                Mathf.Pow(camCon.lockedEnemy.transform.position.x - model.transform.position.x, 2) +
                Mathf.Pow(camCon.lockedEnemy.transform.position.y - model.transform.position.y, 2) +
                Mathf.Pow(camCon.lockedEnemy.transform.position.z - model.transform.position.z, 2));
            text = string.Format("喵！锁定最近敌人！\n目标：{0}\n距离：{1}", camCon.lockedEnemy.name, distance.ToString("f2"));
            textArea2.GetComponent<Text>().text = text;
            lockedTargetDot.rectTransform.position =
                    Vector3.Slerp(
                        lockedTargetDot.rectTransform.position,
                        Camera.main.WorldToScreenPoint
                        (
                            new Vector3
                            (
                                camCon.lockedEnemy.transform.position.x,
                                0,
                                camCon.lockedEnemy.transform.position.z
                            ) + new Vector3
                            (
                                0,
                                camCon.lockedEnemyHalfHeight,
                                0
                            )
                        ),
                        0.1f);
            lockedTargetDot.enabled = true;
            // camCon.lockedTargetDot.transform = 
        }
        else
        {
            print("成功执行等待主人锁定");
            textArea2.GetComponent<Text>().text = "等待主人锁定喵~";
            lockedTargetDot.enabled = false;
        }
    }

}
