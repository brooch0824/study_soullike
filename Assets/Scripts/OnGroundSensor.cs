using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnGroundSensor : MonoBehaviour
{
    public CapsuleCollider capCol;
    public float offset;

    private Vector3 point0;
    private Vector3 point1;

    void Awake()
    {
    }

    void FixedUpdate()
    {
        point0 = transform.position + transform.up * (-0.2f);//下面那个圆心
        point1 = transform.position + transform.up * (capCol.height - 0.2f);//上面那个圆心
        Collider[] outputCols = Physics.OverlapCapsule(point0, point1, 0.01f, LayerMask.GetMask("Ground"));
        if (outputCols.Length != 0)
        {
            // foreach (var col in outputCols)
            // {
            //     print(col.name);
            // }
            SendMessageUpwards("IsGround");

        }
        else SendMessageUpwards("IsNotGround");


    }
}
