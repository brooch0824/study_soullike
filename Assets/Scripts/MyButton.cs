using UnityEngine;

public class MyButton
{
    public bool OnPressed = false;//按钮按下的那一帧
    public bool OnReleased = false;//按钮释放后的那一帧
    public bool IsPressing = false;//按钮是否按着
    public bool IsExtending = false;//按钮释放后、即OnReleased发生后的时间的讯号，用来反馈计时器的true/false（不太确定，我觉得有点多余）


    private bool curState = false;
    private bool lastState = false;
    private MyTimer extTimer = new MyTimer();

    public void Tick(bool Input)
    {
        extTimer.Tick();//计时器


        curState = Input;
        IsPressing = curState;

        OnPressed = false;
        OnReleased = false;
        if (curState != lastState)
        {
            if (curState == true)
            {
                OnPressed = true;
            }
            else
            {
                OnReleased = true;
                StartTimer(extTimer, 0.15f);
            }
        }
        lastState = curState;

        IsExtending = extTimer.state == MyTimer.STATE.RUN ? true : false;


    }

    /// <summary>
    /// 计时器开启（开启代表着只有一帧执行就好了喔）
    /// </summary>
    /// <param name="myTimer"></param>
    /// <param name="duration"></param>
    void StartTimer(MyTimer myTimer, float duration)
    {
        myTimer.duration = duration;
        myTimer.Go();
    }


}
