using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IUserInput : MonoBehaviour
{


    [Header("======== Output Signals ========")]
    public float Dup;//实际速度Y
    public float Dright;
    public float Dmag;
    public float JX;
    public float JY;
    public Vector3 Dvec;
    public float targetScroll;


    public bool run = false;//是否奔跑
    public bool jump = false;//是否跳跃
    public bool roll = false;//是否翻滚
    public bool attack = false;//是否攻击
    public bool defense = false;//是否举盾
    public bool tryLock = false;//是否攻击

    [Header("======== Others ========")]
    public bool canMove = true;//是否允许上下左右的移动输入
    public bool isCollision;
    public float[] animLayerSetBase = new float[2];
    public float[] animLayerSetAttack = new float[2];
    public float[] animLayerSetDefense = new float[2];
    public float layerWeight;
    public float layerWeightLerp;
    public string layerName;

    protected float targetDup;//目标速度
    protected float targetDright;
    protected float velocityDup;//当前速度
    protected float velocityDright;


    /// <summary>
    /// 椭圆映射法：将方坐标转化为圆坐标
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    protected Vector2 SquareToCircle(Vector2 input)
    {
        Vector2 output = Vector2.zero;

        output.x = input.x * Mathf.Sqrt(1 - (input.y * input.y) / 2.0f);
        output.y = input.y * Mathf.Sqrt(1 - (input.x * input.x) / 2.0f);
        return output;

    }
}
